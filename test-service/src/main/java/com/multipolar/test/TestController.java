package com.multipolar.test;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import io.swagger.annotations.ApiOperation;

@Controller
@RequestMapping("/api/test")
public class TestController {

	@ApiOperation(notes = "Validate userid & password", value = "none")
	@RequestMapping(value = "/otp", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public String send(@RequestParam("p") String phoneNumber, @RequestParam("m") String message) {
		
		return "ok";
	}
	
}
